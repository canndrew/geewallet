# <img src="https://github.com/knocte/geewallet/raw/master/logo.png" width="50" /> geewallet

Welcome!

geewallet is a minimalistic and pragmatist crossplatform lightweight opensource brainwallet for people that want to hold the most important cryptocurrencies in the same application with ease and peace of mind.

[![Licence](https://img.shields.io/github/license/knocte/geewallet.svg)](https://github.com/knocte/geewallet/blob/master/LICENCE.txt)

| Branch            | Description                                                            | CI status (build & test suite)                                                                                                                                                |
| ----------------- | ---------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| stable (v0.2.x)   | Console frontend. Currencies supported: BTC, LTC, ETC, ETH             | Linux: [![Linux CI pipeline status badge](https://gitlab.com/knocte/geewallet/badges/stable/pipeline.svg)](https://gitlab.com/knocte/geewallet/commits/stable)      |
| master (v0.3.x)   | main branch where ongoing development takes place (unstable)           | Linux: [![Linux CI pipeline status badge](https://gitlab.com/knocte/geewallet/badges/master/pipeline.svg)](https://gitlab.com/knocte/geewallet/commits/master) <br/>macOS: [![macOS CI pipeline status badge](https://github.com/knocte/geewallet/workflows/macOS/badge.svg)](https://github.com/knocte/geewallet/commits/master) <br/>Windows: [![Windows CI pipeline status badge](https://github.com/knocte/geewallet/workflows/windows/badge.svg)](https://github.com/knocte/geewallet/commits/master) |
| frontend (v0.4.x) | + Xamarin.Forms frontends (Android & iOS & Gtk & macOS & UWP)          | Linux: [![Linux CI pipeline status badge](https://gitlab.com/knocte/geewallet/badges/frontend/pipeline.svg)](https://gitlab.com/knocte/geewallet/commits/frontend)  |

[![Balances mobile-page screenshot](https://raw.githubusercontent.com/knocte/geewallet/master/img/screenshots/maciosandroid-balances.png)](https://raw.githubusercontent.com/knocte/geewallet/master/img/screenshots/maciosandroid-balances.png)

Note: this is a deprecated version of the "ReadMe" file, head here to read the updated version from the frontend branch:
https://gitlab.com/knocte/geewallet/blob/frontend/ReadMe.md
